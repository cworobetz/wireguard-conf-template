**WireGuard Conf Template**

Templates used to set up a simple hub-spoke type VPN deployment. 
Technically WireGuard is peer-to-peer, however this is more of a client-server 
model where clients connect to a singleserver via its public IP and route all 
traffic through that server.